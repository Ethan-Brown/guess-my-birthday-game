from random import randint

# Asks users name "Hi! What is your name? "

user_name = input("Hi! What is your name?")

# Allow the computer to guess 5 times -> loop 5 times
for guess_number in range(5):
# Tries to guess your birth month and year
    print("Guess", guess_number + 1, ":", user_name, "were you born in", randint(1, 12), "/", randint(1924, 2004), "?")
# Prompts with "yes or no?"
    yes_or_no = input("yes or no?")
# If the computer guesses it correctly because you respond yes, it prints the message I knew it! and stops guessing
    if yes_or_no == "yes":
        print("I knew it!")
        exit()
# If the computer guesses incorrectly because you respond no, it prints the message Drat! Lemme try again!
    elif guess_number == 4:
        break
    else:
        print("Drat! Lemme try again!")

# if it's only guessed 1, 2, 3, or 4 times. Otherwise, it prints the message I have other things to do. Good bye.
print("I have other things to do. Goodbye.")
